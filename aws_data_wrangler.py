#!/usr/bin/env python
# coding: utf-8

# # Make sql files in chucnks of 500 per year

# In[ ]:


import pandas as pd
import numpy as np
import glob

FILE = 'compustat_sample_cashtags.xlsx'
if len(glob.glob("*.sql")) != 117:
    df = pd.read_excel(FILE)
    IFS = 500
    years = list(range(2012, 2021))   
    tags = list(set(df.cashtag.values))
    chunks = len(tags)//IFS + 1
    tag_groups = np.array_split(np.array(tags), chunks)
    with open('reserved_words.txt') as f:
        reserved_words = set(f.read().split())

    for year in years:
        for t_idx, tags in enumerate(tag_groups, start=1):
            query = """SELECT year,
                    month,
                    day,
                    CONCAT(month, '/', day, '/', year) AS date, """
            for idx, tag in enumerate(list(tags), start=1):
                as_name = tag[1:]
                if as_name in reserved_words:
                    as_name = f'_{as_name}'
                query += f"count_if (regexp_like(extended_full_text, '(?i)(^|\s)\{tag}\\b')) as {as_name}"
                if idx < len(list(tags)):
                    query += ','
                query += '\n'                       
            query += f"""FROM "transformed"."transformed"
            WHERE year = '{year}'
            GROUP BY  year, month, day
            ORDER BY  year, month, day;"""

            with open(f'{year}_{t_idx}.sql', 'w') as f:
                f.write(query)
else:
    print('sql files already processed')



# In[ ]:


import os
print(os.popen('ls -la *.sql').read())


# # Set up aws-data-wrangler with federated auth
# 
# ```
#     python ~/projects/aws-federated-auth/aws-federated-auth.py --account 063038240025 --rolename AdministratorAccess
#     Username: zamechek
#     Password: 
#     Enter Duo passcode, or 'return' for approved push request: 
# 
#     INFO:botocore.credentials:Found credentials in shared credentials file: ~/.aws/credentials
#     PROFILE NAME                                                      MAX DURATION ACCOUNT NUMBER ROLE NAME   
#     wharton-tweet-collection-AdministratorAccess                             43200 063038240025   AdministratorAccess
# ```
# 

# In[ ]:


import boto3
my_session = boto3.Session(region_name="us-east-1", profile_name='wharton-tweet-collection-AdministratorAccess')


# In[ ]:


import awswrangler as wr
dbs = wr.catalog.databases(boto3_session=my_session)
dbs


# In[ ]:


dbs.iloc[1].Database


# In[ ]:


completed_files = set(glob.glob("*.csv"))
for file in glob.glob("*.sql"):
    output_filename = file.replace('sql', 'csv')
    if output_filename in completed_files:
        print(f"Skipping\t {output_filename}")
        continue
    print(file)
    with open(file) as f:
        query = f.read()
    df = wr.athena.read_sql_query(query, database=dbs.iloc[1].Database, boto3_session=my_session)
    df.to_csv(output_filename)


# In[ ]:




