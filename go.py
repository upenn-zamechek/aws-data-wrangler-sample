import pandas as pd
import numpy as np

FILE = 'compustat_sample_cashtags.xlsx'


df = pd.read_excel(FILE)
IFS = 500
years = list(range(2012, 2021))   
tags = list(set(df.cashtag.values))
chunks = len(tags)//IFS + 1
print(chunks)
tag_groups = np.array_split(np.array(tags), chunks)
print(len(tag_groups))
with open('reserved_words.txt') as f:
    reserved_words = set(f.read().split())

for year in years:
    for t_idx, tags in enumerate(tag_groups, start=1):
        query = """SELECT year,
                month,
                day,
                CONCAT(month, '/', day, '/', year) AS date, """
        for idx, tag in enumerate(list(tags), start=1):
            as_name = tag[1:]
            if as_name in reserved_words:
                as_name = f'_{as_name}'
            query += f"count_if (regexp_like(extended_full_text, '(?i)(^|\s)\{tag}\\b')) as {as_name}"
            if idx < len(list(tags)):
                query += ','
            query += '\n'
        query += f"""FROM "transformed"."transformed"
        WHERE year = {year}
        GROUP BY  year, month, day
        ORDER BY  year, month, day;"""

        with open(f'{year}_{t_idx}.sql', 'w') as f:
            f.write(query)
